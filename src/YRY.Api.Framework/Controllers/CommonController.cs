﻿using DH.Core.Infrastructure;
using DH.Helpers;
using DH.RateLimter;
using DH.Web.Framework;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using NewLife.Caching;

using Pek.Helpers;
using Pek.Models;
using Pek.MVC;
using Pek.Swagger;

using YRY.Api.Framework.Utilities;

using Policy = DH.RateLimter.Policy;

namespace YRY.Api.Framework.Controllers;

/// <summary>
/// 公共接口
/// </summary>
[Produces("application/json")]
[CustomRoute(ApiVersions.V1)]
[Authorize("jwt")]
public class CommonController : ApiControllerBaseX {

    /// <summary>
    /// 验证码
    /// </summary>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("GetVierificationCode")]
    //[ApiSignature]
    [RateValve(Policy = Policy.Ip, Limit = 600, Duration = 3600)]
    public IActionResult GetVierificationCode()
    {
        var result = new DGResult();

        var code = Randoms.RandomStr(4);
        var data = new
        {
            img = VierificationCodeHelpers.CreateBase64Image(code),
            uuid = Guid.NewGuid()
        };

        var _cache = EngineContext.Current.Resolve<ICache>();
        _cache.Set(data.uuid.ToString(), code, 300);

        result.Code = StateCode.Ok;
        result.Data = data;
        return result;
    }
}
