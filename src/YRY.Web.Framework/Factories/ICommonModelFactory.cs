﻿using DH.Web.Framework.Models.Common;

namespace DH.Web.Framework.Factories;

/// <summary>
/// 表示常见模型工厂的接口
/// </summary>
public partial interface ICommonModelFactory {

    /// <summary>
    /// 准备图标模型
    /// </summary>
    /// <returns>
    /// 表示异步操作的任务
    /// 任务结果包含图标模型
    /// </returns>
    Task<FaviconAndAppIconsModel> PrepareFaviconAndAppIconsModelAsync();

}
