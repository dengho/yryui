﻿using DH.Web.Framework.Models;

namespace DH.Web.Framework.Factories;

/// <summary>
/// 表示小部件模型工厂的接口
/// </summary>
public partial interface IWidgetModelFactory
{
    /// <summary>
    /// 获取渲染小部件模型
    /// </summary>
    /// <param name="widgetZone">小工具区域名称</param>
    /// <param name="additionalData">附加数据对象</param>
    /// <returns>
    /// 表示异步操作的任务
    /// 任务结果包含渲染小部件模型的列表
    /// </returns>
    Task<List<RenderWidgetModel>> PrepareRenderWidgetModelAsync(string widgetZone, object additionalData = null);
}