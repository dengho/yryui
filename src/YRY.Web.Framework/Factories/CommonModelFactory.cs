﻿using DH.Core.Domain.Common;
using DH.Web.Framework.Models.Common;

namespace DH.Web.Framework.Factories;

/// <summary>
/// 表示常见模型工厂
/// </summary>
public partial class CommonModelFactory : ICommonModelFactory {

    private readonly CommonSettings _commonSettings;

    public CommonModelFactory(CommonSettings commonSettings
        )
    {
        _commonSettings = commonSettings;
    }

    /// <summary>
    /// 准备图标模型
    /// </summary>
    /// <returns>
    /// 表示异步操作的任务
    /// 任务结果包含图标模型
    /// </returns>
    public virtual Task<FaviconAndAppIconsModel> PrepareFaviconAndAppIconsModelAsync()
    {
        var model = new FaviconAndAppIconsModel
        {
            HeadCode = _commonSettings.FaviconAndAppIconsHeadCode
        };

        return Task.FromResult(model);
    }

}
