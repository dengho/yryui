﻿using DH.Caching;
using DH.Core;
using DH.Core.Infrastructure;
using DH.Services.Cms;
using DH.Web.Framework.Infrastructure.Cache;
using DH.Web.Framework.Models;
using DH.Web.Framework.Themes;

using Microsoft.AspNetCore.Routing;

using NewLife.Caching;

using XCode.Membership;

namespace DH.Web.Framework.Factories;

/// <summary>
/// 获取渲染小部件模型
/// </summary>
public partial class WidgetModelFactory : IWidgetModelFactory {
    protected readonly IThemeContext _themeContext;
    protected readonly IWorkContext _workContext;
    protected readonly IStoreContext _storeContext;
    protected readonly IWidgetPluginManager _widgetPluginManager;

    public WidgetModelFactory(IThemeContext themeContext,
        IStoreContext storeContext,
        IWidgetPluginManager widgetPluginManager,
        IWorkContext workContext)
    {
        _themeContext = themeContext;
        _workContext = workContext;
        _widgetPluginManager = widgetPluginManager;
        _storeContext = storeContext;
    }

    /// <summary>
    /// 获取渲染小部件模型
    /// </summary>
    /// <param name="widgetZone">小工具区域名称</param>
    /// <param name="additionalData">附加数据对象</param>
    /// <returns>
    /// 表示异步操作的任务
    /// 任务结果包含渲染小部件模型的列表
    /// </returns>
    public virtual async Task<List<RenderWidgetModel>> PrepareRenderWidgetModelAsync(string widgetZone, object additionalData = null)
    {
        var theme = await _themeContext.GetWorkingThemeNameAsync();
        var customer = _workContext.CurrentCustomer;
        var customerRoleIds = Role.FindByID(customer?.User?.RoleID ?? 0);
        var store = _storeContext.CurrentStore;

        var cacheKey = CacheHelper.PrepareKeyForShortTermCache(DHModelCacheDefaults.WidgetModelKey,
                customerRoleIds, store, widgetZone, theme);

        var _cache = EngineContext.Current.Resolve<ICache>();
        var cachedModels = _cache.GetOrAdd<IEnumerable<RenderWidgetModel>>(cacheKey.Key, (e) =>
        {
            var list = _widgetPluginManager.LoadActivePluginsAsync(customer?.User, store.Id, widgetZone).Result;

            return list
                .Select(widget => new RenderWidgetModel
                {
                    WidgetViewComponent = widget.GetWidgetViewComponent(widgetZone),
                    WidgetViewComponentArguments = new RouteValueDictionary { ["widgetZone"] = widgetZone }
                });
        }, cacheKey.CacheTime * 60);

        // widget 模型的 “WidgetViewComponentArguments” 属性依赖于 “additionalData”。
        // 我们需要在修改之前克隆缓存的模型（更新后的模型不应该被缓存）
        var models = cachedModels.Select(renderModel => new RenderWidgetModel
        {
            WidgetViewComponent = renderModel.WidgetViewComponent,
            WidgetViewComponentArguments = new RouteValueDictionary { ["widgetZone"] = widgetZone, ["additionalData"] = additionalData }
        }).ToList();

        return models;
    }
}
