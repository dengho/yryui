﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace DH.Web.Framework.TagHelpers.Shared;

/// <summary>
/// "dh-antiforgery-token" tag helper
/// </summary>
[HtmlTargetElement("dh-antiforgery-token", TagStructure = TagStructure.WithoutEndTag)]
public partial class DHAntiForgeryTokenTagHelper : TagHelper {
    #region Properties

    protected IHtmlGenerator Generator { get; set; }

    /// <summary>
    /// 查看上下文
    /// </summary>
    [HtmlAttributeNotBound]
    [ViewContext]
    public ViewContext ViewContext { get; set; }

    #endregion

    #region Ctor

    public DHAntiForgeryTokenTagHelper(IHtmlGenerator generator) {
        Generator = generator;
    }

    #endregion

    #region Methods

    /// <summary>
    /// 使用给定的上下文和输出异步执行标记助手
    /// </summary>
    /// <param name="context">包含与当前HTML标记关联的信息</param>
    /// <param name="output">用于生成HTML标记的有状态HTML元素</param>
    /// <returns>表示异步操作的任务</returns>
    public override Task ProcessAsync(TagHelperContext context, TagHelperOutput output) {
        if (context == null)
            throw new ArgumentNullException(nameof(context));

        if (output == null)
            throw new ArgumentNullException(nameof(output));

        // 清除输出
        output.SuppressOutput();

        // 生成 antiforgery令牌
        var antiforgeryTag = Generator.GenerateAntiforgery(ViewContext);
        if (antiforgeryTag != null)
            output.Content.SetHtmlContent(antiforgeryTag);

        return Task.CompletedTask;
    }

    #endregion
}
