﻿using DH.Web.Framework.Extensions;

using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace DH.Web.Framework.TagHelpers;

/// <summary>
/// 标记助手扩展
/// </summary>
public static class TagHelperExtensions {
    #region Methods

    /// <summary>
    /// 从标记助手输出中获取字符串值
    /// </summary>
    /// <param name="output">与当前HTML标记关联的信息</param>
    /// <param name="attributeName">属性的名称</param>
    /// <returns>Matching name</returns>
    public static async Task<string> GetAttributeValueAsync(this TagHelperOutput output, string attributeName) {

        if (string.IsNullOrEmpty(attributeName) || !output.Attributes.TryGetAttribute(attributeName, out var attr))
            return null;

        if (attr.Value is string stringValue)
            return stringValue;

        return attr.Value switch {
            HtmlString htmlString => htmlString.ToString(),
            IHtmlContent content => await content.RenderHtmlContentAsync(),
            _ => default
        };
    }

    /// <summary>
    /// 从标记助手输出中获取作为键/字符串值对集合的属性
    /// </summary>
    /// <param name="output">用于生成HTML标记的有状态HTML元素</param>
    /// <returns>键/字符串值对的集合</returns>
    public static async Task<IDictionary<string, string>> GetAttributeDictionaryAsync(this TagHelperOutput output) {
        if (output is null)
            throw new ArgumentNullException(nameof(output));

        var result = new Dictionary<string, string>();

        if (output.Attributes.Count == 0)
            return result;

        foreach (var attrName in output.Attributes.Select(x => x.Name).Distinct()) {
            result.Add(attrName, await output.GetAttributeValueAsync(attrName));
        }

        return result;
    }

    #endregion
}