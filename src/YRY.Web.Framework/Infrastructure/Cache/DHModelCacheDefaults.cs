﻿using DH.Core.Caching;

namespace DH.Web.Framework.Infrastructure.Cache;

public static partial class DHModelCacheDefaults
{

    /// <summary>
    /// 小部件信息键
    /// </summary>
    /// <remarks>
    /// {0} : 当前客户角色ID哈希
    /// {1} : 当前店铺ID
    /// {2} : 小工具区域
    /// {3} : 当前主题名称
    /// </remarks>
    public static CacheKey WidgetModelKey => new("DH.pres.widget-{0}-{1}-{2}-{3}", WidgetPrefixCacheKey);

    public static string WidgetPrefixCacheKey => "DH.pres.widget";
}
