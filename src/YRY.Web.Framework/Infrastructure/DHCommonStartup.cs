﻿using DH.AspNetCore.Middleware;
using DH.Core.Infrastructure;
using DH.Core.Webs;
using DH.MVC.Routing;
using DH.Services.Membership;
using DH.Services.WebMiddleware;
using DH.Web.Framework.Infrastructure.Extensions;
using DH.Web.Framework.Mvc.Routing;

using NewLife;
using NewLife.Log;

using Pek.Infrastructure;
using Pek.VirtualFileSystem;
using Pek.Webs;

using XCode.DataAccessLayer;

namespace DH.Web.Framework.Infrastructure;

/// <summary>
/// 表示用于在应用程序启动时配置公共功能和中间件的对象
/// </summary>
public partial class DHCommonStartup : IDHStartup
{
    /// <summary>
    /// 添加并配置任何中间件
    /// </summary>
    /// <param name="services">服务描述符集合</param>
    /// <param name="configuration">应用程序的配置</param>
    /// <param name="webHostEnvironment"></param>
    public void ConfigureServices(IServiceCollection services, IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
    {
        // 添加选项功能
        services.AddOptions();

        // 添加分布式缓存
        services.AddDistributedCache();

        // 添加HTTP会话状态功能
        services.AddHttpSession();

        // 添加默认HTTP客户端
        services.AddDHHttpClients();

        // 添加防伪
        services.AddAntiForgery();

        // 添加主题支持
        services.AddThemes();

        // 添加路由
        services.AddRouting(options =>
        {
            // 为语言添加约束键
            options.ConstraintMap[DHRoutingDefaults.LanguageParameterTransformer] = typeof(LanguageParameterTransformer);
        });
    }

    /// <summary>
    /// 配置添加的中间件的使用
    /// </summary>
    /// <param name="application">用于配置应用程序的请求管道的生成器</param>
    public void Configure(IApplicationBuilder application)
    {
        // 配置静态Http上下文访问器
        application.UseStaticHttpContext();

        // 调整系统表名
        FixAppTableName();
        FixAvatar();
        WebHelper2.FixTenantMenu();

        // 使用管理提供者
        application.UseManagerProvider();

        // 检查请求的页面是否为保持活动页面
        application.UseKeepAlive();

        // 检查数据库是否已安装
        application.UseInstallUrl();

        // 使用HTTP会话
        application.UseSession();

        // 使用请求本地化
        application.UseDHRequestLocalization();

        var webHostEnvironment = EngineContext.Current.Resolve<IWebHostEnvironment>();
        if (!webHostEnvironment.IsDevelopment())
        {
            if (DHSetting.Current.AllSslEnabled)
            {
                // HSTS的默认值为30天。 您可能要针对生产方案更改此设置，请参见https://aka.ms/aspnetcore-hsts。
                application.UseHsts();
                application.UseHttpsRedirection();
            }
        }

        application.UseMiddleware<TranslateMiddleware>();  // 简繁转换

        // 获取请求的内容和参数
        application.UseRequestLogger();
    }

    private static void FixAppTableName()
    {
        try
        {
            var dal = DAL.Create("DH");
            var tables = dal.Tables;
            if (tables != null && !tables.Any(e => e.TableName.EqualIgnoreCase("OAuthApp")))
            {
                XTrace.WriteLine("未发现OAuth应用新表 OAuthApp");

                // 验证表名和部分字段名，避免误改其它表
                var dt = tables.FirstOrDefault(e => e.TableName.EqualIgnoreCase("App"));
                if (dt != null && dt.Columns.Any(e => e.ColumnName.EqualIgnoreCase("RoleIds")))
                {
                    XTrace.WriteLine("发现OAuth应用旧表 App ，准备重命名");

                    var rs = dal.Execute($"Alter Table App Rename To OAuthApp");
                    XTrace.WriteLine("重命名结果：{0}", rs);
                }
            }
        }
        catch (Exception ex)
        {
            XTrace.WriteException(ex);
        }
    }

    /// <summary>
    /// 配置虚拟文件系统
    /// </summary>
    /// <param name="options">虚拟文件配置</param>
    public void ConfigureVirtualFileSystem(DHVirtualFileSystemOptions options)
    {
    }

    /// <summary>
    /// 注册路由
    /// </summary>
    /// <param name="endpoints">路由生成器</param>
    public void UseDHEndpoints(IEndpointRouteBuilder endpoints)
    {
    }

    /// <summary>
    /// 将区域路由写入数据库
    /// </summary>
    public void ConfigureArea()
    {

    }

    /// <summary>
    /// 调整菜单
    /// </summary>
    public void ChangeMenu()
    {

    }

    /// <summary>
    /// 升级处理逻辑
    /// </summary>
    public void Update()
    {

    }

    /// <summary>
    /// 配置使用添加的中间件
    /// </summary>
    /// <param name="application">用于配置应用程序的请求管道的生成器</param>
    public void ConfigureMiddleware(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// UseRouting前执行的数据
    /// </summary>
    /// <param name="application"></param>
    /// <exception cref="NotImplementedException"></exception>
    public void BeforeRouting(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// UseAuthentication或者UseAuthorization后面 Endpoints前执行的数据
    /// </summary>
    /// <param name="application"></param>
    public void AfterAuth(IApplicationBuilder application)
    {

    }

    /// <summary>修正头像附件，移动到附件上传目录</summary>
    private static void FixAvatar()
    {
        var set = DHSetting.Current;
        if (set.AvatarPath.IsNullOrEmpty()) return;

        var av = set.AvatarPath.CombinePath("User").AsDirectory();
        if (!av.Exists) return;

        // 如果附件目录跟头像目录一致，则不需要移动
        var dst = set.UploadPath.CombinePath("User").AsDirectory();
        if (dst.FullName.EqualIgnoreCase(av.FullName)) return;

        try
        {
            // 确保目标目录存在
            dst.Parent.FullName.EnsureDirectory(false);

            //av.MoveTo(dst.FullName);
            //av.CopyTo(dst.FullName, null, true, e => XTrace.WriteLine("移动 {0}", e));

            // 来源目录根，用于截断
            var root = av.FullName.EnsureEnd(Path.DirectorySeparatorChar.ToString());
            foreach (var item in av.GetAllFiles(null, true))
            {
                var name = item.FullName.TrimStart(root);
                var dfile = dst.FullName.CombinePath(name);
                if (!File.Exists(dfile))
                {
                    XTrace.WriteLine("移动 {0}", name);
                    item.MoveTo(dfile.EnsureDirectory(true), false);
                }
                else
                {
                    item.Delete();
                }

                //if (item.Exists) item.Delete();
                //if (File.Exists(item.FullName)) File.Delete(item.FullName);
            }

            // 删除空目录
            if (!av.GetAllFiles(null, true).Any()) av.Delete(true);
        }
        catch (Exception ex)
        {
            XTrace.WriteException(ex);
        }
    }

    /// <summary>
    /// 获取此启动配置实现的顺序
    /// </summary>
    public int StartupOrder => 100; // 应在错误处理程序之后加载公共服务

    /// <summary>
    /// 获取此启动配置实现的顺序。主要针对ConfigureMiddleware、UseRouting前执行的数据、UseAuthentication或者UseAuthorization后面 Endpoints前执行的数据
    /// </summary>
    public Int32 ConfigureOrder => 0;
}
