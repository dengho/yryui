﻿using DH.Entity;

using Microsoft.AspNetCore.Rewrite;

using Pek.Infrastructure;
using Pek.VirtualFileSystem;

namespace DH.Web.Framework.Infrastructure;

/// <summary>
/// 表示应用程序启动时配置路由的类
/// </summary>
public partial class DHUrlRewriteStartup : IDHStartup
{
    /// <summary>
    /// 添加并配置任何中间件
    /// </summary>
    /// <param name="services">服务描述符集合</param>
    /// <param name="configuration">应用程序的配置</param>
    /// <param name="webHostEnvironment"></param>
    public void ConfigureServices(IServiceCollection services, IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
    {
    }

    /// <summary>
    /// 配置添加的中间件的使用
    /// </summary>
    /// <param name="application">用于配置应用程序的请求管道的生成器</param>
    public void Configure(IApplicationBuilder application)
    {
        // 重定向
        var options = new RewriteOptions();

        var RewriteList = RouteRewrite.GetAll();
        var language = Language.FindByStatus();

        foreach (var item in RewriteList)
        {
            var regexInfo = item.RegexInfo;
            options.AddRewrite("(?i)" + regexInfo.Replace("{language}/", ""), item.ReplacementInfo.Replace("{language}/", ""), skipRemainingRules: true);

            foreach (var item1 in language)
            {
                options.AddRewrite("(?i)" + regexInfo.Replace("{language}", item1.UniqueSeoCode), item.ReplacementInfo.Replace("{language}", item1.UniqueSeoCode), skipRemainingRules: true);
            }
        }

        application.UseRewriter(options);
    }

    /// <summary>
    /// 配置虚拟文件系统
    /// </summary>
    /// <param name="options">虚拟文件配置</param>
    public void ConfigureVirtualFileSystem(DHVirtualFileSystemOptions options)
    {
    }

    /// <summary>
    /// 注册路由
    /// </summary>
    /// <param name="endpoints">路由生成器</param>
    public void UseDHEndpoints(IEndpointRouteBuilder endpoints)
    {
    }

    /// <summary>
    /// 将区域路由写入数据库
    /// </summary>
    public void ConfigureArea()
    {

    }

    /// <summary>
    /// 调整菜单
    /// </summary>
    public void ChangeMenu()
    {

    }

    /// <summary>
    /// 升级处理逻辑
    /// </summary>
    public void Update()
    {

    }

    /// <summary>
    /// 配置使用添加的中间件
    /// </summary>
    /// <param name="application">用于配置应用程序的请求管道的生成器</param>
    public void ConfigureMiddleware(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// UseRouting前执行的数据
    /// </summary>
    /// <param name="application"></param>
    /// <exception cref="NotImplementedException"></exception>
    public void BeforeRouting(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// UseAuthentication或者UseAuthorization后面 Endpoints前执行的数据
    /// </summary>
    /// <param name="application"></param>
    public void AfterAuth(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// 获取此启动配置实现的顺序
    /// </summary>
    public int StartupOrder => 98; // URL重写中间件应在静态文件之前注册

    /// <summary>
    /// 获取此启动配置实现的顺序。主要针对ConfigureMiddleware、UseRouting前执行的数据、UseAuthentication或者UseAuthorization后面 Endpoints前执行的数据
    /// </summary>
    public Int32 ConfigureOrder => 0;
}
