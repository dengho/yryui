﻿using DH.AspNetCore.Cookies;
using DH.Cookies;
using DH.Core;
using DH.Core.Caching;
using DH.Core.Configuration;
using DH.Core.Events;
using DH.Services.Cms;
using DH.Services.Common;
using DH.Services.Customers;
using DH.Services.Events;
using DH.Services.Helpers;
using DH.Services.Localization;
using DH.Services.Membership;
using DH.Services.Plugins;
using DH.Services.Plugins.Marketplace;
using DH.Services.ScheduleTasks;
using DH.Services.Seo;
using DH.Services.Services;
using DH.Services.Themes;
using DH.Web.Framework.Admin.Controllers;
using DH.Web.Framework.Mvc.Routing;
using DH.Web.Framework.Services;
using DH.Web.Framework.Themes;
using DH.Web.Framework.UI;

using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection.Extensions;

using NewLife.Caching;

using Pek.Infrastructure;
using Pek.VirtualFileSystem;

using YRY.Web.Controllers.Areas.Admin;
using YRY.Web.Controllers.Common;

namespace DH.Web.Framework.Infrastructure;

/// <summary>
/// 表示应用程序启动时注册服务
/// </summary>
public partial class DHStartup : IDHStartup
{
    /// <summary>
    /// 文件提供程序
    /// </summary>
    /// <param name="application">用于配置应用程序的请求管道的生成器</param>
    public void Configure(IApplicationBuilder application)
    {
    }

    /// <summary>
    /// 添加并配置任何中间件
    /// </summary>
    /// <param name="services">服务描述符集合</param>
    /// <param name="configuration">应用程序的配置</param>
    /// <param name="webHostEnvironment"></param>
    public void ConfigureServices(IServiceCollection services, IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
    {
        // 文件提供程序
        services.AddScoped<IDHFileProvider, DHFileProvider>();

        // web助手
        services.AddScoped<IWebHelper, WebHelper>();

        // UserAgent帮助
        services.AddScoped<IUserAgentHelper, UserAgentHelper>();

        // 插件
        services.AddScoped<IPluginService, PluginService>();
        services.AddScoped<OfficialFeedManager>();

        // 静态缓存管理器
        var appSettings = Singleton<AppSettings>.Instance;
        var distributedCacheConfig = appSettings.Get<DistributedCacheConfig>();
        if (distributedCacheConfig.Enabled)
        {
            switch (distributedCacheConfig.DistributedCacheType)
            {
                case DistributedCacheType.Memory:
                    services.AddScoped<ILocker, MemoryDistributedCacheManager>();
                    services.AddScoped<IStaticCacheManager, MemoryDistributedCacheManager>();
                    break;
                case DistributedCacheType.SqlServer:
                    services.AddScoped<ILocker, MsSqlServerCacheManager>();
                    services.AddScoped<IStaticCacheManager, MsSqlServerCacheManager>();
                    break;
                case DistributedCacheType.Redis:
                    services.AddScoped<ILocker, RedisCacheManager>();
                    services.AddScoped<IStaticCacheManager, RedisCacheManager>();
                    break;
            }
        }
        else
        {
            services.AddSingleton<ILocker, MemoryCacheManager>();
            services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();
        }

        // 工作上下文
        services.AddScoped<IWorkContext, WebWorkContext>();

        // 站点上下文
        services.AddScoped<IStoreContext, WebStoreContext>();

        // Cookie
        services.AddScoped<ICookie, Cookie>();

        // https跳转
        if (DHSetting.Current.AllSslEnabled)
        {
            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status301MovedPermanently;
            });
        }

        ////默认注入缓存实现  在外部项目实现，因为每个项目的情况不太一样
        //services.TryAddSingleton<ICacheProvider, CacheProvider>();

        // 系统应用缓存，小于10000数据的可以考虑直接使用Cache.Default
        if (!services.Any(e => e.ServiceType == typeof(ICache)))
        {
            if (DHUtilSetting.Current.IsUseRedisCache)
            {
                var redisConn = new FullRedis(DHUtilSetting.Current.RedisConnectionString, DHUtilSetting.Current.RedisPassWord, DHUtilSetting.Current.RedisDatabaseId);

                services.TryAddSingleton<ICache>(redisConn);
            }
            else
            {
                var cache = new MemoryCache();
                services.TryAddSingleton<ICache>(cache);
            }
        }
            

        #region 跨域
        services.AddCors(options => options.AddPolicy(CubeService.corsPolicy, builder =>
        {
            var corsUrl = DHSetting.Current.CORSUrl.Length > 0 ? DHSetting.Current.CORSUrl : "*";

            if (corsUrl != "*")
            {
                builder
                  .SetIsOriginAllowedToAllowWildcardSubdomains()
                  .WithOrigins(corsUrl.Split(","))
                  .AllowAnyMethod()
                  .AllowCredentials()
                  .AllowAnyHeader()
                  .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding")
                  .Build();
            }
            else
            {
                builder
                  .SetIsOriginAllowed(_ => true)
                  .AllowAnyMethod()
                  .AllowCredentials()
                  .AllowAnyHeader()
                  .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding")
                  .Build();
            }
        }));
        #endregion

        services.AddScoped<IGenericAttributeService, GenericAttributeService>();
        services.AddScoped<ICustomerService, CustomerService>();
        services.AddScoped<IDHHtmlHelper, DHHtmlHelper>();
        services.AddScoped<ILocalizationService, LocalizationService>();
        services.AddScoped<IUrlRecordService, UrlRecordService>();
        services.AddScoped<IThemeProvider, ThemeProvider>();
        services.AddScoped<IThemeContext, ThemeContext>();
        services.AddSingleton<IRoutePublisher, RoutePublisher>();
        services.AddSingleton<IEventPublisher, EventPublisher>();


        //plugin managers
        services.AddScoped(typeof(IPluginManager<>), typeof(PluginManager<>));
        services.AddScoped<IWidgetPluginManager, WidgetPluginManager>();

        services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

        // 注册所有设置
        var typeFinder = Singleton<ITypeFinder>.Instance;

        // 分段路由处理
        if (DHSetting.Current.IsInstalled)
            services.AddScoped<SlugRouteTransformer>();

        // 计划任务
        services.AddSingleton<ITaskScheduler, DH.Services.ScheduleTasks.TaskScheduler>();
        services.AddTransient<IScheduleTaskRunner, ScheduleTaskRunner>();

        // 事件消费者
        var consumers = typeFinder.FindClassesOfType(typeof(IConsumer<>)).ToList();
        foreach (var consumer in consumers)
            foreach (var findInterface in consumer.FindInterfaces((type, criteria) =>
            {
                var isMatch = type.IsGenericType && ((Type)criteria).IsAssignableFrom(type.GetGenericTypeDefinition());
                return isMatch;
            }, typeof(IConsumer<>)))
                services.AddScoped(findInterface, consumer);

        // 添加魔方模块
        // 添加管理提供者
        services.AddManageProvider();

        // 添加数据保护，优先在外部支持Redis持久化，这里默认使用数据库持久化
        //if (services.Any(e => e.ServiceType == typeof(FullRedis) || e.ServiceType == typeof(ICacheProvider) && e.ImplementationType == typeof(RedisCacheProvider)))
        //    services.AddDataProtection().PersistKeysToRedis();
        //else
        //    services.AddDataProtection().PersistKeysToDb();
        // 添加数据保护
        services.AddDataProtection()
            .PersistKeysToDb();

        // 服务
        services.AddSingleton<PasswordService>();
        services.AddSingleton<UserService>();
        services.AddSingleton<AccessService>();
    }

    /// <summary>
    /// 配置虚拟文件系统
    /// </summary>
    /// <param name="options">虚拟文件配置</param>
    public void ConfigureVirtualFileSystem(DHVirtualFileSystemOptions options)
    {
    }

    /// <summary>
    /// 注册路由
    /// </summary>
    /// <param name="endpoints">路由生成器</param>
    public void UseDHEndpoints(IEndpointRouteBuilder endpoints)
    {
    }

    /// <summary>
    /// 将区域路由写入数据库
    /// </summary>
    public void ConfigureArea()
    {
        AreaBase.SetRoute<LoginController>(AdminArea.AreaName);
    }

    /// <summary>
    /// 调整菜单
    /// </summary>
    public void ChangeMenu()
    {

    }

    /// <summary>
    /// 升级处理逻辑
    /// </summary>
    public void Update()
    {

    }

    /// <summary>
    /// 配置使用添加的中间件
    /// </summary>
    /// <param name="application">用于配置应用程序的请求管道的生成器</param>
    public void ConfigureMiddleware(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// UseRouting前执行的数据
    /// </summary>
    /// <param name="application"></param>
    /// <exception cref="NotImplementedException"></exception>
    public void BeforeRouting(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// UseAuthentication或者UseAuthorization后面 Endpoints前执行的数据
    /// </summary>
    /// <param name="application"></param>
    public void AfterAuth(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// 获取此启动配置实现的顺序
    /// </summary>
    public int StartupOrder => 2000;

    /// <summary>
    /// 获取此启动配置实现的顺序。主要针对ConfigureMiddleware、UseRouting前执行的数据、UseAuthentication或者UseAuthorization后面 Endpoints前执行的数据
    /// </summary>
    public Int32 ConfigureOrder => 0;
}
