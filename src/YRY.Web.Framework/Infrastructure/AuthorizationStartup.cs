﻿using System.Text;

using DH.Common;
using DH.Permissions.Authorization.Policies;
using DH.Permissions.Extensions;

using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Tokens;

using Pek.Infrastructure;
using Pek.Security;
using Pek.VirtualFileSystem;

namespace DH.Web.Framework.Infrastructure;

/// <summary>
/// 表示应用程序启动时配置授权中间件的对象
/// </summary>
public partial class AuthorizationStartup : IDHStartup
{
    /// <summary>
    /// 添加并配置任何中间件
    /// </summary>
    /// <param name="services">服务描述符集合</param>
    /// <param name="configuration">应用程序的配置</param>
    /// <param name="webHostEnvironment"></param>
    public void ConfigureServices(IServiceCollection services, IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
    {
        #region Jwt认证
        var jwtOptions = DHSetting.Current.JwtOptions;
        services.AddAuthorization(options =>
        {
            options.AddPolicy("jwt", policy => policy.Requirements.Add(new JsonWebTokenAuthorizationRequirement()));
        }).AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, o =>
        {
            o.LoginPath = new PathString("/Login");
            o.AccessDeniedPath = new PathString("/Site/Error/Forbidden");
        }).AddJwtBearer(o =>
        {
            o.TokenValidationParameters = GetValidationParameters(jwtOptions);
            o.SaveToken = true;
        }).AddBasic();
        services.AddJwt(options =>
        {
            options.Secret = DHSetting.Current.JwtOptions.Secret;
            options.Issuer = DHSetting.Current.JwtOptions.Issuer;
            options.Audience = DHSetting.Current.JwtOptions.Audience;
            options.AccessExpireMinutes = DHSetting.Current.JwtOptions.AccessExpireMinutes;
            options.RefreshExpireMinutes = DHSetting.Current.JwtOptions.RefreshExpireMinutes;
            options.ThrowEnabled = DHSetting.Current.JwtOptions.ThrowEnabled;
        });

        #endregion
    }

    /// <summary>
    /// 获取验证参数
    /// </summary>
    /// <param name="options">配置</param>
    /// <returns></returns>
    private static TokenValidationParameters GetValidationParameters(JwtOptions options)
    {
        return new TokenValidationParameters()
        {
            ValidateIssuerSigningKey = true, // 是否验证发行者签名密钥
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.Secret)),
            ValidateIssuer = true, // 是否验证发行者
            ValidIssuer = options.Issuer,
            ValidateAudience = true, // 是否验证接收者
            ValidAudience = options.Audience,
            ValidateLifetime = true, // 是否验证超时
            LifetimeValidator = (before, expires, token, param) => expires > DateTime.UtcNow,
            ClockSkew = TimeSpan.FromSeconds(30), // 缓冲过期时间，总的有效时间等于该时间加上Jwt的过期时间，如果不配置，则默认是5分钟
            RequireExpirationTime = true
        };
    }

    /// <summary>
    /// 配置添加的中间件的使用
    /// </summary>
    /// <param name="application">用于配置应用程序的请求管道的生成器</param>
    public void Configure(IApplicationBuilder application)
    {
        // 添加授权中间件
        application.UseAuthentication();  // 用于Jwt检验
        application.UseAuthorization();   // 用于自定义
    }

    /// <summary>
    /// 配置虚拟文件系统
    /// </summary>
    /// <param name="options">虚拟文件配置</param>
    public void ConfigureVirtualFileSystem(DHVirtualFileSystemOptions options)
    {
    }

    /// <summary>
    /// 注册路由
    /// </summary>
    /// <param name="endpoints">路由生成器</param>
    public void UseDHEndpoints(IEndpointRouteBuilder endpoints)
    {
    }

    /// <summary>
    /// 将区域路由写入数据库
    /// </summary>
    public void ConfigureArea()
    {

    }

    /// <summary>
    /// 调整菜单
    /// </summary>
    public void ChangeMenu()
    {

    }

    /// <summary>
    /// 升级处理逻辑
    /// </summary>
    public void Update()
    {

    }

    /// <summary>
    /// 配置使用添加的中间件
    /// </summary>
    /// <param name="application">用于配置应用程序的请求管道的生成器</param>
    public void ConfigureMiddleware(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// UseRouting前执行的数据
    /// </summary>
    /// <param name="application"></param>
    /// <exception cref="NotImplementedException"></exception>
    public void BeforeRouting(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// UseAuthentication或者UseAuthorization后面 Endpoints前执行的数据
    /// </summary>
    /// <param name="application"></param>
    public void AfterAuth(IApplicationBuilder application)
    {

    }

    /// <summary>
    /// 获取此启动配置实现的顺序
    /// </summary>
    public int StartupOrder => 700; // 应在端点之前和身份验证之后加载授权

    /// <summary>
    /// 获取此启动配置实现的顺序。主要针对ConfigureMiddleware、UseRouting前执行的数据、UseAuthentication或者UseAuthorization后面 Endpoints前执行的数据
    /// </summary>
    public Int32 ConfigureOrder => 0;
}

/// <summary>
/// 授权控制器模型转换器
/// </summary>
public class AuthorizeControllerModelConvention : IControllerModelConvention {
    /// <summary>
    /// 实现Apply
    /// </summary>
    public void Apply(ControllerModel controller)
    {
        controller.Filters.Add(new AuthorizeFilter("jwt"));
    }
}
