﻿using System.Reflection;

using Autofac.Extensions.DependencyInjection;

using DH.Configs;
using DH.Core.Configuration;
using DH.Web.Framework.Infrastructure.Extensions;

using Microsoft.Extensions.DependencyInjection.Extensions;

using NewLife;
using NewLife.Caching;
using NewLife.Configuration;
using NewLife.IP;
using NewLife.Log;

using Stardust;
using Stardust.Registry;

using XCode;
using XCode.DataAccessLayer;

namespace DH.Web.Framework;

/// <summary>基类服务</summary>
public static class CubeService
{
    public readonly static string corsPolicy = "CorsPolicy";

    #region 配置基类
    /// <summary>添加基类</summary>
    /// <param name="builder"></param>
    /// <param name="configuration"></param>
    /// <param name="webHostEnvironment"></param>
    /// <returns></returns>
    public static IServiceCollection AddCube(this WebApplicationBuilder builder,
        IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
    {
        // 引入星尘
        TryAddStardust(builder.Services);

        // 检查是否延迟启动，可能是重启或更新
        var args = Environment.GetCommandLineArgs();
        if ("-delay".EqualIgnoreCase(args)) Thread.Sleep(3000);

        using var span = DefaultTracer.Instance?.NewSpan(nameof(AddCube));

        XTrace.WriteLine("{0} Start 配置系统 {0}", new String('=', 32));
        Assembly.GetExecutingAssembly().WriteVersion();

        builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
        builder.Configuration.AddJsonFile(DHConfigurationDefaults.AppSettingsFilePath, true, true);
        if (!string.IsNullOrEmpty(builder.Environment?.EnvironmentName))
        {
            var path = string.Format(DHConfigurationDefaults.AppSettingsEnvironmentFilePath, builder.Environment.EnvironmentName);
            builder.Configuration.AddJsonFile(path, true, true);
        }
        builder.Configuration.AddEnvironmentVariables();
        builder.Configuration.SetConfig();  // 可在Settings文件夹中放入多个配置文件

        var set = DHSetting.Current;
        builder.Services.AddSingleton(set);

        // 指定启动网址
        if (!set.Urls.IsNullOrWhiteSpace())
        {
            builder.WebHost.UseUrls(set.Urls);
        }

        // 连接字符串
        DAL.ConnStrs.TryAdd("DH", "MapTo=Membership");
        DAL.ConnStrs.TryAdd("DH", "MapTo=DG");



        // 向应用程序添加服务并配置服务提供商
        builder.Services.ConfigureApplicationServices(builder, builder.Environment);

        XTrace.WriteLine("{0} End   配置系统 {0}", new String('=', 32));

        return builder.Services;
    }

    private static void TryAddStardust(IServiceCollection services)
    {
        if (!services.Any(e => e.ServiceType == typeof(StarFactory)))
        {
            var star = new StarFactory();
            //services.AddSingleton(star);
            //services.AddSingleton(P => star.Tracer);
            //services.AddSingleton(P => star.Config);
            //services.AddSingleton(p => star.Service);

            // 替换为混合配置提供者，优先本地配置
            var old = JsonConfigProvider.LoadAppSettings();
            star.SetLocalConfig(old);

            services.TryAddSingleton(star);
            services.TryAddSingleton(p => star.Tracer ?? DefaultTracer.Instance ?? (DefaultTracer.Instance ??= new DefaultTracer()));
            //services.AddSingleton(p => star.Config);
            services.TryAddSingleton(p => star.Service!);

            // 替换为混合配置提供者，优先本地配置
            services.TryAddSingleton(p => star.GetConfig()!);

            // 分布式缓存
            //services.TryAddSingleton<ICacheProvider, CacheProvider>();
            services.TryAddSingleton(XTrace.Log);
            services.TryAddSingleton(typeof(ICacheProvider), typeof(CacheProvider));
        }
    }

    #endregion

    #region 使用基类

    /// <summary>使用基类</summary>
    /// <param name="app"></param>
    /// <param name="env"></param>
    /// <param name="configuration"></param>
    /// <returns></returns>
    public static IApplicationBuilder UseCube(this IApplicationBuilder app,
        IConfiguration configuration, IWebHostEnvironment env = null)
    {
        var provider = app.ApplicationServices;
        using var span = DefaultTracer.Instance?.NewSpan(nameof(UseCube));

        XTrace.WriteLine("{0} Start 初始化系统 {0}", new String('=', 32));

        // 初始化数据库连接
        var set = DHSetting.Current;
        if (set.IsNew)
            EntityFactory.InitAll();
        else
            EntityFactory.InitAllAsync();

        // 注册IP地址库
        IpResolver.Register();

        // 配置应用程序HTTP请求管道
        app.ConfigureRequestPipeline();
        app.StartEngine();

        XTrace.WriteLine("{0} End   初始化系统 {0}", new String('=', 32));
        XTrace.WriteLine("Welcome to use DH系统");

        Task.Run(() => ResolveStarWeb(provider));

        // 注册退出事件
        if (app is IHost web)
            NewLife.Model.Host.RegisterExit(() =>
            {
                XTrace.WriteLine("魔方优雅退出！");
                web.StopAsync().Wait();
            });

        return app;
    }

    /// <summary>修正头像附件，移动到附件上传目录</summary>
    private static void FixAvatar()
    {
        var set = DHSetting.Current;
        if (set.AvatarPath.IsNullOrEmpty()) return;

        var av = set.AvatarPath.CombinePath("User").AsDirectory();
        if (!av.Exists) return;

        // 如果附件目录跟头像目录一致，则不需要移动
        var dst = set.UploadPath.CombinePath("User").AsDirectory();
        if (dst.FullName.EqualIgnoreCase(av.FullName)) return;

        try
        {
            // 确保目标目录存在
            dst.Parent.FullName.EnsureDirectory(false);

            //av.MoveTo(dst.FullName);
            //av.CopyTo(dst.FullName, null, true, e => XTrace.WriteLine("移动 {0}", e));

            // 来源目录根，用于截断
            var root = av.FullName.EnsureEnd(Path.DirectorySeparatorChar.ToString());
            foreach (var item in av.GetAllFiles(null, true))
            {
                var name = item.FullName.TrimStart(root);
                var dfile = dst.FullName.CombinePath(name);
                if (!File.Exists(dfile))
                {
                    XTrace.WriteLine("移动 {0}", name);
                    item.MoveTo(dfile.EnsureDirectory(true), false);
                }
                else
                {
                    item.Delete();
                }

                //if (item.Exists) item.Delete();
                //if (File.Exists(item.FullName)) File.Delete(item.FullName);
            }

            // 删除空目录
            if (!av.GetAllFiles(null, true).Any()) av.Delete(true);
        }
        catch (Exception ex)
        {
            XTrace.WriteException(ex);
        }
    }

    static async Task ResolveStarWeb(IServiceProvider provider)
    {
        // 消费StarWeb服务地址，如果未接入星尘，这里也没必要获取这个地址
        var registry = provider.GetService<IRegistry>();
        if (registry != null)
        {
            using var span = DefaultTracer.Instance?.NewSpan(nameof(ResolveStarWeb));
            try
            {
                var webs = await registry.ResolveAddressAsync("StarWeb");
                if (webs != null)
                {
                    XTrace.WriteLine("StarWeb: {0}", webs.Join());
                    //StarHelper.StarWeb = webs.FirstOrDefault();
                    if (webs.Length > 0)
                    {
                        // 保存到配置文件
                        var set = DHSetting.Current;
                        set.StarWeb = webs[0];
                        set.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                span?.SetError(ex, null);
                XTrace.WriteLine(ex.Message);
            }
        }
    }

    #endregion
}
