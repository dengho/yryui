﻿using DH.Services.Components;
using DH.Web.Framework.Factories;

using Microsoft.AspNetCore.Mvc;

namespace DH.Web.Framework.Components;

public partial class WidgetViewComponent : DHViewComponent
{
    protected readonly IWidgetModelFactory _widgetModelFactory;

    public WidgetViewComponent(IWidgetModelFactory widgetModelFactory)
    {
        _widgetModelFactory = widgetModelFactory;
    }

    public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData = null)
    {
        var model = await _widgetModelFactory.PrepareRenderWidgetModelAsync(widgetZone, additionalData);

        // 没有数据
        if (!model.Any())
            return Content("");

        return View(model);
    }
}
