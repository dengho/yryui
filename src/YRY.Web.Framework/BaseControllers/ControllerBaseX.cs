﻿using DH.AspNetCore.Webs;
using DH.Cookies;
using DH.Core;
using DH.Core.Infrastructure;
using DH.Core.Webs;
using DH.Entity;
using DH.Helpers;
using DH.Models;
using DH.Services.MVC.Filters;
using DH.Web.Framework.Controllers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

using NewLife;

using System.Dynamic;

namespace DH.Web.Framework;

/// <summary>
/// 控制器基类
/// </summary>
[HttpsRequirement]
public class ControllerBaseX : BaseController
{
    #region 属性

    /// <summary>临时会话扩展信息。仅限本地内存，不支持分布式共享</summary>
    public IDictionary<String, Object> Session { get; protected set; }

    /// <summary>
    /// 用户惟一标识符
    /// </summary>
    public Int64 Sid { get; set; }

    /// <summary>
    /// Cookie操作类
    /// </summary>
    public ICookie DGCookies { get { return EngineContext.Current.Resolve<ICookie>(); } }

    /// <summary>用户主机</summary>
    public String UserHost => HttpContext.GetUserHost();

    /// <summary>
    /// 当前语言简称
    /// </summary>
    public String CurrentLanguage { get; set; }

    /// <summary>
    /// 当前语言
    /// </summary>
    public Language WorkingLanguage { get; set; }

    /// <summary>
    /// 当前站点
    /// </summary>
    public SiteInfo CurrentSiteInfo
    {
        get
        {
            var StoreContext = EngineContext.Current.Resolve<IStoreContext>();
            return StoreContext.CurrentStore;
        }
    }

    #endregion

    #region 构造

    public ControllerBaseX()
    {
        #region Sid处理
        try
        {
            Sid = DGCookies.GetValue<Int64>(DHSetting.Current.SidName);
        }
        catch
        {
            // 生成Sid
            Sid = Id.GetSId();
        }
        if (Sid <= 0)
        {
            // 生成Sid
            Sid = Id.GetSId();
            DGCookies.SetValue(DHSetting.Current.SidName, Sid);
        }
        else
        {
            DGCookies.SetValue(DHSetting.Current.SidName, Sid);
        }
        Pek.Webs.HttpContext.Current.Items[DHSetting.Current.SidName] = Sid;
        #endregion

        var _workContext = EngineContext.Current.Resolve<IWorkContext>();
        WorkingLanguage = _workContext.WorkingLanguage;
        Pek.Webs.HttpContext.Current.Items["WorkingLanguage"] = WorkingLanguage;

        CurrentLanguage = DGCookies.GetValue<String>(DHSetting.Current.LangName);
        if (CurrentLanguage.IsNullOrWhiteSpace())
        {
            CurrentLanguage = WorkingLanguage.UniqueSeoCode;
            DGCookies.SetValue(DHSetting.Current.LangName, CurrentLanguage);
        }
        else if (CurrentLanguage != WorkingLanguage.UniqueSeoCode)
        {
            CurrentLanguage = WorkingLanguage.UniqueSeoCode;
            DGCookies.SetValue(DHSetting.Current.LangName, CurrentLanguage);
        }
    }

    #endregion

    public SiteSettings SiteSettings
    {
        get
        {
            return SiteSettingInfo.SiteSettings;
        }
    }

    protected ViewResult View404()
    {
        Response.StatusCode = 404;

        dynamic model = new ExpandoObject();
        model.SiteUrl = DHWeb.GetSiteUrl();

        var siteInfo = SiteInfo.GetDefaultSeo();
        model.Site = siteInfo;
        model.SiteName = siteInfo.SiteName;

        var ViewData = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
        {
            Model = model
        };
        var TempData = EngineContext.Current.Resolve<ITempDataDictionaryFactory>()?.GetTempData(HttpContext);
        var view = new ViewResult
        {
            ViewName = "404",
            ViewData = ViewData,
            TempData = TempData
        };
        return view;
    }

    /// <summary>
    /// 根据指定的ResourceKey属性获取资源字符串。
    /// </summary>
    /// <param name="resourceKey">代表ResourceKey的字符串</param>
    /// <returns>代表请求的资源字符串的翻译内容</returns>
    protected String GetResource(String resourceKey)
    {
        return LocaleStringResource.GetResource(resourceKey);
    }

    /// <summary>
    /// 根据指定的ResourceKey属性获取资源字符串。
    /// </summary>
    /// <param name="resourceKey">代表ResourceKey的字符串</param>
    /// <param name="Lng">语言数据</param>
    /// <returns>代表请求的资源字符串的翻译内容</returns>
    protected String GetResource(String resourceKey, String Lng)
    {
        if (Lng.IsNullOrWhiteSpace()) return LocaleStringResource.GetResource(resourceKey);
        return LocaleStringResource.GetResource(resourceKey, Lng);
    }

    protected Exception GerInnerException(Exception ex)
    {
        if (ex.InnerException != null)
        {
            return GerInnerException(ex.InnerException);
        }
        else
        {
            return ex;
        }
    }

    #region 兼容处理
    /// <summary>获取请求值</summary>
    /// <param name="key"></param>
    /// <returns></returns>
    protected virtual String GetRequest(String key) => Request.GetRequestValue(key);
    #endregion

}
