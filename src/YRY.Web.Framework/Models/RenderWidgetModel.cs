﻿using DH.Models;

namespace DH.Web.Framework.Models;

public partial record RenderWidgetModel : BaseModel
{
    public Type WidgetViewComponent { get; set; }
    public object WidgetViewComponentArguments { get; set; }
}