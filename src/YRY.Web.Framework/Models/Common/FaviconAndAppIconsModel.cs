﻿using Pek;

namespace DH.Web.Framework.Models.Common;

public partial record FaviconAndAppIconsModel : BaseDHModel {
    public string HeadCode { get; set; }
}