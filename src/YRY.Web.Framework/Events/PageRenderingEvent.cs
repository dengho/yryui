﻿using DH.Web.Framework.UI;

namespace DH.Web.Framework.Events;

/// <summary>
/// 表示页面呈现事件
/// </summary>
public partial class PageRenderingEvent
{
    #region Ctor

    /// <summary>
    /// 实例化
    /// </summary>
    /// <param name="helper">HTML帮助程序</param>
    /// <param name="overriddenRouteName">覆盖的路由名称</param>
    public PageRenderingEvent(IDHHtmlHelper helper, string overriddenRouteName = null)
    {
        Helper = helper;
        OverriddenRouteName = overriddenRouteName;
    }

    #endregion

    #region Properties

    /// <summary>
    /// 获取HTML帮助程序
    /// </summary>
    public IDHHtmlHelper Helper { get; protected set; }

    /// <summary>
    /// 获取重写的路由名称
    /// </summary>
    public string OverriddenRouteName { get; protected set; }

    #endregion

    #region Methods

    /// <summary>
    /// 获取与呈现此页面的请求关联的路由名称
    /// </summary>
    /// <param name="handleDefaultRoutes">一个值，指示是否使用引擎信息生成名称，除非另有指定</param>
    /// <returns>路由名称</returns>
    public string GetRouteName(bool handleDefaultRoutes = false)
    {
        // 如果指定了重写的路由名称，则使用该名称
        // 当某些自定义页面使用自定义路由时，我们使用它来指定自定义路由名称。但我们仍然需要调用此事件
        if (!string.IsNullOrEmpty(OverriddenRouteName))
            return OverriddenRouteName;

        // 或尝试获取已注册的端点路由名称
        return Helper.GetRouteName(handleDefaultRoutes);
    }

    #endregion
}
