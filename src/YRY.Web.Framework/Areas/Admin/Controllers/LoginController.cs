﻿using Microsoft.AspNetCore.Mvc;

using System.ComponentModel;

using YRY.Web.Controllers.Areas.Admin;

namespace DH.Web.Framework.Admin.Controllers;

/// <summary>
/// 后台登录
/// </summary>
[AdminArea]
public class LoginController : BaseAdminControllerX {

    /// <summary>
    /// 登录
    /// </summary>
    /// <returns></returns>
    [DisplayName("登录")]
    public IActionResult Index()
    {
        return View();
    }
}
