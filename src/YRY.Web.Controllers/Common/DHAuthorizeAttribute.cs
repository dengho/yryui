﻿using DH;
using DH.AspNetCore.Webs;
using DH.Cookies;
using DH.Core.Infrastructure;
using DH.Core.Webs;
using DH.Entity;
using DH.Services.Membership;
using DH.Webs;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;

using NewLife;
using NewLife.Caching;
using NewLife.Log;

using System.Reflection;
using System.Web;

using XCode.Membership;

using YRY.Web.Controllers.Areas.Admin;

namespace YRY.Web.Controllers;

/// <summary>授权特性</summary>
public class DHAuthorizeAttribute : Attribute, IAuthorizationFilter {
    /// <summary>
    /// 是否ajax
    /// </summary>
    public Boolean IsAjax { get; set; }

    #region 构造

    static DHAuthorizeAttribute()
    {
        XTrace.WriteLine("注册过滤器：{0}", typeof(DHAuthorizeAttribute).FullName);
    }

    /// <summary>实例化实体授权特性</summary>
    public DHAuthorizeAttribute() { }

    #endregion

    /// <summary>授权发生时触发</summary>
    /// <param name="filterContext"></param>
    public void OnAuthorization(AuthorizationFilterContext filterContext)
    {
        /*
         * 验证范围：
         * 2，所有带有DGAuthorizeAttribute特性的控制器或动作
         */
        var act = filterContext.ActionDescriptor;
        var ctrl = (ControllerActionDescriptor)act;

        // 允许匿名访问时，直接跳过检查
        if (
            ctrl.MethodInfo.IsDefined(typeof(AllowAnonymousAttribute)) ||
            ctrl.ControllerTypeInfo.IsDefined(typeof(AllowAnonymousAttribute))) return;

        // 如果已经处理过，就不处理了
        if (filterContext.Result != null) return;

        if (!AuthorizeCore(filterContext.HttpContext))
        {
            HandleUnauthorizedRequest(filterContext);
        }
    }

    /// <summary>授权核心</summary>
    /// <param name="httpContext"></param>
    /// <returns></returns>
    protected Boolean AuthorizeCore(Microsoft.AspNetCore.Http.HttpContext httpContext)
    {
        var prv = ManageProvider.Provider;

        // 判断当前登录用户
        var user = ManagerProviderHelper.TryLogin(prv, httpContext);
        if (user == null) return false;

        if (user.Enable) return true;

        return false;
    }

    /// <summary>未认证请求</summary>
    /// <param name="filterContext"></param>
    protected void HandleUnauthorizedRequest(AuthorizationFilterContext filterContext)
    {
        // 来到这里，有可能没登录，有可能没权限
        var prv = ManageProvider.Provider;

        var retUrl = filterContext.HttpContext.Request.GetEncodedPathAndQuery();
        //.Host.ToString();//.Url?.PathAndQuery;

        var ms = OAuthConfig.GetValids(GrantTypes.AuthorizationCode);
        ms = ms.Where(e => e.Name == "ChuangChu").ToList();

        if (ms != null && ms.Count == 1 && !DHSetting.Current.AllowLogin)
        {
            var _cache = EngineContext.Current.Resolve<ICache>();
            var _cookie = EngineContext.Current.Resolve<ICookie>();

            var returnUrl = filterContext.HttpContext.Request.GetRequestValue("r");

            if (returnUrl.IsNullOrWhiteSpace())
            {
                returnUrl = WebHelper2.GetRawUrlStr(filterContext.HttpContext.Request);
            }

            var url = $"~/Sso/Login?name={HttpUtility.UrlEncode(ms[0].Name)}";
            if (!returnUrl.IsNullOrEmpty()) url += "&r=" + HttpUtility.UrlEncode(returnUrl);

            filterContext.Result = new RedirectResult(url);
        }
        else
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest() || IsAjax)
            {
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.Result = new JsonResult(new { code = 401, msg = "没有登陆或登录超时！" });
            }
            else
            {
                var rurl = String.Empty;

                if (DHSetting.Current.IsOnlyManager)
                {
                    rurl = $"~/{AdminArea.AreaName}/Login".AppendReturn(retUrl).ToLower();
                }
                else
                {
                    rurl = DHSetting.Current.LoginUrl.AppendReturn(retUrl).ToLower();
                }

                filterContext.Result = new RedirectResult(rurl);
            }
        }
    }
}
