﻿using DH.Helpers;

using Microsoft.AspNetCore.Mvc;

using NewLife;

namespace YRY.Web.Controllers;

/// <summary>
/// 跳转控制器，一般和于跳转外部链接
/// </summary>
public class JumpController : Controller {
    public IActionResult Index(String Url)
    {
        if (Url.IsNullOrWhiteSpace()) return Content("参数不完全");

        Url = Url.UrlDecode();

        if (Url.StartsWithIgnoreCase("http:") || Url.StartsWithIgnoreCase("https:"))
        {
            return Redirect(Url);
        }

        return Redirect($"http://{Url}");
    }
}
