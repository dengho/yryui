﻿using DH.Domain.Blogs;
using DH.Services.Components;

using Microsoft.AspNetCore.Mvc;

namespace YRY.FastAdmin.Components;

public partial class BlogRssHeaderLinkViewComponent : DHViewComponent {
    private readonly BlogSettings _blogSettings;

    public BlogRssHeaderLinkViewComponent(BlogSettings blogSettings)
    {
        _blogSettings = blogSettings;
    }

    public IViewComponentResult Invoke(int currentCategoryId, int currentProductId)
    {
        if (!_blogSettings.Enabled || !_blogSettings.ShowHeaderRssUrl)
            return Content("");

        return View();
    }
}