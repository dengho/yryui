﻿using DH.Domain.News;
using DH.Services.Components;

using Microsoft.AspNetCore.Mvc;

namespace YRY.FastAdmin.Components;

public partial class NewsRssHeaderLinkViewComponent : DHViewComponent {
    private readonly NewsSettings _newsSettings;

    public NewsRssHeaderLinkViewComponent(NewsSettings newsSettings)
    {
        _newsSettings = newsSettings;
    }

    public IViewComponentResult Invoke(int currentCategoryId, int currentProductId)
    {
        if (!_newsSettings.Enabled || !_newsSettings.ShowHeaderRssUrl)
            return Content("");

        return View();
    }
}
