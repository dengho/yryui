﻿using DH.Services.Components;
using DH.Web.Framework.Factories;

using Microsoft.AspNetCore.Mvc;

namespace YRY.FastAdmin.Components;

public partial class FaviconViewComponent : DHViewComponent {
    private readonly ICommonModelFactory _commonModelFactory;

    public FaviconViewComponent(ICommonModelFactory commonModelFactory)
    {
        _commonModelFactory = commonModelFactory;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
        var model = await _commonModelFactory.PrepareFaviconAndAppIconsModelAsync();
        if (string.IsNullOrEmpty(model.HeadCode))
            return Content("");
        return View(model);
    }
}